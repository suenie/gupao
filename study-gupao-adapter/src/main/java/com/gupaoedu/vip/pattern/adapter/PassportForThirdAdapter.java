package com.gupaoedu.vip.pattern.adapter;

import com.gupaoedu.vip.pattern.adapter.adapters.LoginAdapter;
import com.gupaoedu.vip.pattern.adapter.adapters.LoginForQQAdapter;
import com.gupaoedu.vip.pattern.adapter.adapters.LoginForWechatAdapter;

/**
 * 适配器模式：  已经存在的类，它方法和需求不匹配的情况，但是结果相似或者相同。
 * 适配器模式不是软件开发阶段需要考虑的的设计模式，是随着软件的维护由于不同产品，不同厂家
 * 造成功能类似而接口不相同的情况的解决方案。
 * 例如： 原本最开始只有简单的账号密码登录，后来慢慢有了qq，微信，微博， 等方式。
 * 在兼容账号密码登录的情况下，就需要适配后面的需求。
 * 一个公共的接口或者抽象类的适配器。有两个公共方法，一个判断是否是当前的适配器，一个是执行登录的方法。
 * 不同的适配器的类去实现或者继承公共的适配器。去实现不同的登录逻辑。
 * 新建一个service继承原有的servic类，然后有不同的登录方法，在不同的方法中去调用不同的适配器类进行登录。
 * 因为方法太多，可以用简单工厂模式利用反射去新建实例然后动态去调用适配器的登录方法。
 */
public class PassportForThirdAdapter extends Service implements IPassportForThird{
    public void loginForQQ(String id) {
        process(id, LoginForQQAdapter.class);
    }

    public void loginForWechat(String id) {
        process(id, LoginForWechatAdapter.class);
    }

    @Override
    public void login(String id) {
        super.login(id);
    }

    private void process(String id,Class<? extends LoginAdapter> clazz){
        try{
            LoginAdapter loginAdapter = clazz.newInstance();
            if(loginAdapter.support(loginAdapter)){
                loginAdapter.login(id);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
