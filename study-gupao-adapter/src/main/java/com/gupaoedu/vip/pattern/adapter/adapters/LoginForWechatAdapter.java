package com.gupaoedu.vip.pattern.adapter.adapters;

public class LoginForWechatAdapter implements LoginAdapter {
    public boolean support(Object adapter) {
        return adapter instanceof LoginForWechatAdapter;
    }

    public void login(String id) {
        System.out.println("微信登录    ");
    }
}
