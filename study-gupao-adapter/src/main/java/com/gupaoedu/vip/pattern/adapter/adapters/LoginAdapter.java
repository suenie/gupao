package com.gupaoedu.vip.pattern.adapter.adapters;

public interface LoginAdapter {

    boolean support (Object adapter);

    void login(String id);
}
