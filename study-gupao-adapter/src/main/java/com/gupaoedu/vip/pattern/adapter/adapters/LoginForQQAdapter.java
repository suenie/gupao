package com.gupaoedu.vip.pattern.adapter.adapters;

public class LoginForQQAdapter implements LoginAdapter {
    public boolean support(Object adapter) {
        return adapter instanceof LoginForQQAdapter;
    }

    public void login(String id) {
        System.out.println("qq登录");
    }
}
