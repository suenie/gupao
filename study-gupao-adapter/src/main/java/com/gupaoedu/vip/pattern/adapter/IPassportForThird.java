package com.gupaoedu.vip.pattern.adapter;

public interface IPassportForThird {


    void loginForQQ(String id);

    void loginForWechat(String id);
}
