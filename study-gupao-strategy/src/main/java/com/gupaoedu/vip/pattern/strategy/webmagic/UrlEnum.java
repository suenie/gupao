package com.gupaoedu.vip.pattern.strategy.webmagic;

public enum UrlEnum {

    GOOGLE("GoogleProcessor"),
    BAIDU("BaiduProcessor")
    ;

    UrlEnum(String value){
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }
}
