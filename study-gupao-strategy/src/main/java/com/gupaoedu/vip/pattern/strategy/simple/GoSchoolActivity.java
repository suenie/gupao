package com.gupaoedu.vip.pattern.strategy.simple;

/**
 * 策略模式在会有很多的策略类，如果是常规的ifelse 那么每次写起来就会非常臃肿，而且每次都需要改核心类，容易混淆。
 * 可以通过工厂模式和单例模式来解决，创建一个工厂来来加载所有的策略类。然后每次用户在调用的时候只需要知道调用哪个策略类就行。
 */
public class GoSchoolActivity {

    private StyleStrategy styleStrategy;

    public GoSchoolActivity (StyleStrategy styleStrategy){
        this.styleStrategy = styleStrategy;
    }

    public void excute(){
        styleStrategy.goSchool();
    }

    public static void main(String[] args) {
        //这样的写法会使得维护非常麻烦，不仅要增加新的类，而且每次都要修改原来的代码逻辑。不符合开闭原则。
        //可以通过工厂模式来加载所有的策略，用户只需要传入一个参数，然后直接去调用对应的策略。
        String style = "bus";
        if(style.equals("bus")){
            new GoSchoolActivity(new BusStrategy()).excute();
        }else if(style.equals("car")){
            new GoSchoolActivity(new CarStrategy()).excute();
        }else if(style.equals("bycycle")){
            new GoSchoolActivity(new BycycleStrategy()).excute();
        }
    }
}
