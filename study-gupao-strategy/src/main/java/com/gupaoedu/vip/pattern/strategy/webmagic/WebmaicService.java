package com.gupaoedu.vip.pattern.strategy.webmagic;

public class WebmaicService {

    private PageProcessor pageProcessor;

    public WebmaicService (PageProcessor pageProcessor){
        this.pageProcessor = pageProcessor;
    }

    public void excute(){
        pageProcessor.process();
    }

    public static void main(String[] args) {
        new WebmaicService(new GoogleProcessor()).excute();
    }
}
