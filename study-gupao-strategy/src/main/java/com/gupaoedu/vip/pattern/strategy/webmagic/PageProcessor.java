package com.gupaoedu.vip.pattern.strategy.webmagic;

public interface PageProcessor {

    void process();
}
