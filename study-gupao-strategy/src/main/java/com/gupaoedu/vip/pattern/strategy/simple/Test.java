package com.gupaoedu.vip.pattern.strategy.simple;

public class Test {

    public static void main(String[] args) {
        GoSchoolActivity activity = new GoSchoolActivity(
                StyleStrategyFactory.getStyleStrategy("bus"));
        activity.excute();
    }
}
