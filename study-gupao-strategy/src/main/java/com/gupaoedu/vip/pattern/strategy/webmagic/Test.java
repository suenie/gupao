package com.gupaoedu.vip.pattern.strategy.webmagic;

public class Test {

    public static void main(String[] args) {
        String url = "GOOGLE";
        new WebmaicService((PageProcessor) ProcessorFactory.getProcessor(
                "com.gupaoedu.vip.pattern.strategy.webmagic."+UrlEnum.valueOf(url).getValue())).excute();
    }
}
