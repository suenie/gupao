package com.gupaoedu.vip.pattern.strategy.simple;

import java.util.HashMap;
import java.util.Map;

public class StyleStrategyFactory {

    private static Map<String,StyleStrategy> strategyMap = new HashMap<String, StyleStrategy>();

    static {
        strategyMap.put(StrategyParameter.BUS.getValue(),new BusStrategy());
        strategyMap.put(StrategyParameter.CAR.getValue(),new BycycleStrategy());
        strategyMap.put(StrategyParameter.BYCYCLE.getValue(),new CarStrategy());
        strategyMap.put(StrategyParameter.DEFAULT.getValue(),new BusStrategy());
    }

    private StyleStrategyFactory (){};

    public static StyleStrategy getStyleStrategy(String strategyKey){
        StyleStrategy styleStrategy = strategyMap.get(strategyKey);
        return styleStrategy==null?strategyMap.get(StrategyParameter.DEFAULT.getValue()):styleStrategy;
    }

    private enum StrategyParameter{
        BUS("bus"),CAR("car"),BYCYCLE("bycycle"),DEFAULT("bus");
        private final String value;

        StrategyParameter(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
