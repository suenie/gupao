package com.gupaoedu.vip.pattern.strategy.webmagic;

import java.util.HashMap;
import java.util.Map;

public class ProcessorFactory {

    private static Map<String,Object> processorMap = new HashMap<String, Object>();

    private ProcessorFactory (){};

    public static Object getProcessor(String name){
        if(!processorMap.containsKey(name)){
            Object o = null;
            try {
                o = Class.forName(name).newInstance();
                processorMap.put(name,o);
                return o;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return processorMap.get(name);
    }


}
