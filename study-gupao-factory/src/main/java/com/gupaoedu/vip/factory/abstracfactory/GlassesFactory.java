package com.gupaoedu.vip.factory.abstracfactory;

/**
 * 抽象工厂模式相比于之前的简单工厂模式和工厂方法模式，逻辑更加复杂，引用场景相比于之前多一个维度。
 * 简单工厂模式： 用户需要一个镜片。工厂只提供一个简单的镜片模板给用户使用。但是如果用户有其他要求，
 * 而且， 用户如果需要镜架或者更多要求，就需要不断去修改逻辑，不符合开闭原则。
 *
 * 工厂方法模式：制定一个工厂接口，然后镜片工厂制定镜片，只需要用镜片工厂类去实现接口，有额外逻辑单独放在镜片工厂类中，
 * 需要镜架的话，也是新建一个镜架工厂类，再去实现工厂接口，去生产镜架。易于扩展，但是如果产品太多。会显得结构庞大，维护麻烦。
 *
 * 抽象工厂模式：首先创建一个眼镜工厂接口，再创建一个品牌（例如依视路）眼镜工厂实现眼镜工厂接口，然后再创建镜片接口，
 * 再创建依视路镜片类去实现镜片接口，然后品牌眼镜工厂有生产其品牌的镜片的方法，返回值为依视路镜片类，用镜片接口接收。
 * 当这个品牌需要生产镜架时，只需要添加生产其品牌的镜架的方法，前提是已有镜架的接口。眼镜工厂也有生产镜架的方法。
 * 那么我们就自己扩展实现新的生产镜架的方法。
 * 缺点在于，逻辑会变得复杂，不易理解，眼镜工厂返回的是镜片接口，不是具体的实现。
 *
 */
public interface GlassesFactory {

    ILensGlass createtLensGlasses();

    IFrameGlasses createFrameGlasses();


}
