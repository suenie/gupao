package com.gupaoedu.vip.factory.factorymethod;

import com.gupaoedu.vip.factory.IGlasses;

/***
 * 工厂方法模式，是指定义一个创建对象的接口，但是让其实现的类来决定实例化哪个类，工厂方法让类的实例化推迟到子类中进行。
 * 相比简单工厂来说，在实例化对象前，可以在子类中写逻辑，单独维护，在需要扩展新的类时，只需要新建类去实现创建对象的接口去实例化新的对象。
 * 但是逻辑会比之前复杂，需要维护的类会越来越多。
 */
public interface IGlassesFactory {

    IGlasses create();
}
