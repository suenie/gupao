package com.gupaoedu.vip.factory.abstracfactory;

public interface ILensGlass {

    void produce();
}
