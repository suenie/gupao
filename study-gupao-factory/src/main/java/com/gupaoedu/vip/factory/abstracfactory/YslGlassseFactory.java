package com.gupaoedu.vip.factory.abstracfactory;

public class YslGlassseFactory implements GlassesFactory {
    public ILensGlass createtLensGlasses() {
        return new YslLensGlass();
    }

    public IFrameGlasses createFrameGlasses() {
        return new YslFrameGlasses();
    }
}
