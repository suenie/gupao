package com.gupaoedu.vip.factory.abstracfactory;

public interface IFrameGlasses {

    void produce();
}
