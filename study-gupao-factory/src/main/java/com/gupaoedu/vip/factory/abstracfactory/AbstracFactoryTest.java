package com.gupaoedu.vip.factory.abstracfactory;

public class AbstracFactoryTest {

    public static void main(String[] args) {

        GlassesFactory factory = new YslGlassseFactory();
        factory.createtLensGlasses().produce();
        factory.createFrameGlasses().produce();

    }
}
