package com.gupaoedu.vip.factory.simplefactory;

import com.gupaoedu.vip.factory.IGlasses;
import com.gupaoedu.vip.factory.LensGlasses;

/**
 * 简单工厂模式，通过传入的值让工厂判断需要生产哪个产品。
 * 缺点在于如果产品数量很多，需要做很多的判断，或者在其中有很多复杂的逻辑，那么每次需求变更的时候
 * 需要修改其中的代码，不符合开闭原则。
 * 开闭原则：提供扩展性，但是不允许修改。可以通过继承来扩展，但是不能直接去修改类的逻辑。
 */
public class GlassesFactory {

    public IGlasses create(String name){
        if(name.equals("lens")){
            return new LensGlasses();
        }
        return null;
    }

    public IGlasses create2(String name){
        try {
            if(!("".equals(name)||name==null)){
                return (IGlasses)Class.forName(name).newInstance();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public IGlasses create3(Class clazz){
        try {
            if(clazz!=null){
                return (IGlasses)clazz.newInstance();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
