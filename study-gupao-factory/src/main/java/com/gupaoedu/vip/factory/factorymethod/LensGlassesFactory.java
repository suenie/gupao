package com.gupaoedu.vip.factory.factorymethod;

import com.gupaoedu.vip.factory.IGlasses;
import com.gupaoedu.vip.factory.LensGlasses;

public class LensGlassesFactory implements IGlassesFactory {
    public IGlasses create() {
        return new LensGlasses();
    }
}
