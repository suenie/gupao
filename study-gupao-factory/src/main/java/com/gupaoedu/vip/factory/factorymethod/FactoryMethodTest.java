package com.gupaoedu.vip.factory.factorymethod;

public class FactoryMethodTest {

    public static void main(String[] args) {
        IGlassesFactory factory = new LensGlassesFactory();
        factory.create().produce();
    }
}
