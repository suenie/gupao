package com.gupaoedu.vip.factory.simplefactory;

import com.gupaoedu.vip.factory.IGlasses;
import com.gupaoedu.vip.factory.LensGlasses;

public class SimpleFactoryTest {
    public static void main(String[] args) {
        GlassesFactory factory = new GlassesFactory();
        IGlasses glasses1 = factory.create("lens");
        IGlasses glasses2 = factory.create2("com.gupaoedu.vip.factory.LensGlasses");
        IGlasses glasses3 = factory.create3(LensGlasses.class);
        glasses2.produce();
    }

}
