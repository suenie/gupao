package com.gupaoedu.vip.factory.factorymethod;

import com.gupaoedu.vip.factory.FrameGlasses;
import com.gupaoedu.vip.factory.IGlasses;
import com.gupaoedu.vip.factory.LensGlasses;

public class FrameGlassesFactory implements IGlassesFactory {
    public IGlasses create() {
        return new FrameGlasses();
    }
}
