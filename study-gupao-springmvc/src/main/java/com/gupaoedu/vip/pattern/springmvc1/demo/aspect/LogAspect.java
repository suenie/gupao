package com.gupaoedu.vip.pattern.springmvc1.demo.aspect;

import com.gupaoedu.vip.pattern.springmvc2.aop.aspect.MyJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class LogAspect {

    private Logger logger = LoggerFactory.getLogger(LogAspect.class);

    public void before (MyJoinPoint joinPoint){
        joinPoint.setUserAttribute("startTime_"+joinPoint.getMethod().getName(),System.currentTimeMillis());
        logger.info("Invoker Before Method!!"+
                "\nTargetObject:"+joinPoint.getThis()+
                "\nArgs:"+ Arrays.toString(joinPoint.getArguments()));
    }

    public void after (MyJoinPoint joinPoint){
        logger.info("Invoker After Method!!"+
                "\nTargetObject:"+joinPoint.getThis()+
                "\nArgs:"+ Arrays.toString(joinPoint.getArguments()));
        long startTime = (Long)joinPoint.getUserAttribute("startTime_" + joinPoint.getMethod().getName());
        long endTime = System.currentTimeMillis();
        System.out.println("use time :"+(endTime-startTime));
    }

    public void afterThrow(MyJoinPoint joinPoint,Throwable ex){
        logger.info("出现异常!!"+
                "\nTargetObject:"+joinPoint.getThis()+
                "\nArgs:"+ Arrays.toString(joinPoint.getArguments())+
                "\nThrow:"+ex.getMessage());
    }

}
