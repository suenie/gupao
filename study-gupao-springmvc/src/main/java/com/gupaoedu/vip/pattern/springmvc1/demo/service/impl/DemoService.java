package com.gupaoedu.vip.pattern.springmvc1.demo.service.impl;

import com.gupaoedu.vip.pattern.springmvc1.annotation.MyService;
import com.gupaoedu.vip.pattern.springmvc1.demo.service.IDemoService;

/**
 * 核心业务逻辑
 */
@MyService
public class DemoService implements IDemoService {

	public String get(String name) {
		return "My name is " + name;
	}

}
