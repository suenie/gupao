package com.gupaoedu.vip.pattern.springmvc2.context;

/**
 * 通过解耦方式获得IOC容器的顶层设计
 * 后面将通过监听器扫描所有的类，只要实现了此接口
 * 将自动调用setApplicationContext方法，从而将IOC容器注入到目标类中
 */
public interface MyApplicationContextAware {

    void setApplicationContext(MyApplicationContext applicationContext);
}
