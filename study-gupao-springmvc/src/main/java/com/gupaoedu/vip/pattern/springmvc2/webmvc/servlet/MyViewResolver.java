package com.gupaoedu.vip.pattern.springmvc2.webmvc.servlet;

import java.io.File;
import java.util.Locale;

public class MyViewResolver {

    private File templateDir;

    private static final String DEFAULT_TEMPLATE_SUFFIX = ".html";

    public MyViewResolver(String templatePath) {
        this.templateDir = new File(templatePath);
    }

    public MyView resolveViewName(String viewName, Locale locale)throws Exception{
        if(null == viewName||"".equals(viewName.trim())){return null;}
        viewName = viewName.endsWith(DEFAULT_TEMPLATE_SUFFIX)?viewName:(viewName+DEFAULT_TEMPLATE_SUFFIX);
        File file = new File((this.templateDir.getPath() + "/" + viewName).replaceAll("/+","/"));
        return new MyView(file);
    };
}
