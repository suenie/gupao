package com.gupaoedu.vip.pattern.springmvc2.aop.intercept;

public interface MyMethodInterceptor {

    Object invoke(MyMethodInvocation invocation)throws Throwable;
}
