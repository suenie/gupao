package com.gupaoedu.vip.pattern.springmvc2.beans;

/**
 * 工厂的顶层设计
 */
public interface MyBeanFactory {

    /**
     * 从IOC容器中获取bean
     * @param beanName
     * @return
     */
    Object getBean(String beanName);
}
