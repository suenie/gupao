package com.gupaoedu.vip.pattern.springmvc2.beans.support;

import com.gupaoedu.vip.pattern.springmvc2.beans.config.MyBeandefinition;
import com.gupaoedu.vip.pattern.springmvc2.context.support.MyAbstractApplicationContext;

import java.util.HashMap;
import java.util.Map;

public class MyDefaultListableBeanFactory extends MyAbstractApplicationContext {

    /**
     * IOC容器
     */
    public final Map<String, MyBeandefinition> beandefinitionMap = new HashMap<String, MyBeandefinition>();
}
