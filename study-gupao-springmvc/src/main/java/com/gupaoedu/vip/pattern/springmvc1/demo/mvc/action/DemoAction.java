package com.gupaoedu.vip.pattern.springmvc1.demo.mvc.action;


import com.gupaoedu.vip.pattern.springmvc1.annotation.MyAutowired;
import com.gupaoedu.vip.pattern.springmvc1.annotation.MyController;
import com.gupaoedu.vip.pattern.springmvc1.annotation.MyRequestMapping;
import com.gupaoedu.vip.pattern.springmvc1.annotation.MyRequestParam;
import com.gupaoedu.vip.pattern.springmvc1.demo.service.IDemoService;
import com.gupaoedu.vip.pattern.springmvc2.webmvc.servlet.MyModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


//虽然，用法一样，但是没有功能
@MyController
@MyRequestMapping("/demo")
public class DemoAction {

  	@MyAutowired
	IDemoService demoService;

	@MyRequestMapping("/query.*")
	public MyModelAndView query(HttpServletRequest req, HttpServletResponse resp,
								@MyRequestParam("name") String name){
		String result = demoService.get(name);
		try {
//			resp.getWriter().write(result);
			Map<String, String> model = new HashMap<String, String>();
			model.put("name", name);
			return new MyModelAndView("query",model);
		} catch (Exception e) {
			e.printStackTrace();
			return new MyModelAndView("500");
		}
	}

	@MyRequestMapping("/add")
	public String add(HttpServletRequest req, HttpServletResponse resp,
                    @MyRequestParam("a") Integer a, @MyRequestParam("b") Integer b){
		return a + "+" + b + "=" + (a + b);

	}

	@MyRequestMapping("/sub")
	public void add(HttpServletRequest req, HttpServletResponse resp,
                    @MyRequestParam("a") Double a, @MyRequestParam("b") Double b){
		try {
			resp.getWriter().write(a + "-" + b + "=" + (a - b));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@MyRequestMapping("/remove")
	public String  remove(@MyRequestParam("id") Integer id){
		return "" + id;
	}

}
