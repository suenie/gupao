package com.gupaoedu.vip.pattern.springmvc2.beans.config;

public class MyBeanPostProcessor {

    public Object postProcessBeforeInitialization(Object bean,String beanName) throws Exception{
        return bean;
    }

    public Object postProcessAfterInitialization(Object bean,String beanName) throws Exception{
        return bean;
    }
}
