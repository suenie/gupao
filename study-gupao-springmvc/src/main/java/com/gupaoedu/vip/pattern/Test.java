package com.gupaoedu.vip.pattern;

import com.gupaoedu.vip.pattern.springmvc2.beans.config.MyBeanWrapper;
import com.gupaoedu.vip.pattern.springmvc2.context.MyApplicationContext;

public class Test {

    public static void main(String[] args) {
        MyApplicationContext context = new MyApplicationContext("classpath:application.properties");
        System.out.println(((MyBeanWrapper)context.getBean("demoAction")).getWrappedInstance());
    }
}
