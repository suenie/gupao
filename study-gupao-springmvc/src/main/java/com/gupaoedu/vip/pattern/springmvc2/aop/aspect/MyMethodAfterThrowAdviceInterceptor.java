package com.gupaoedu.vip.pattern.springmvc2.aop.aspect;

import com.gupaoedu.vip.pattern.springmvc2.aop.intercept.MyMethodInterceptor;
import com.gupaoedu.vip.pattern.springmvc2.aop.intercept.MyMethodInvocation;

import java.lang.reflect.Method;

public class MyMethodAfterThrowAdviceInterceptor extends MyAbstractAspectAdvice implements MyMethodInterceptor {

    private String throwName;

    public MyMethodAfterThrowAdviceInterceptor(Method aspectMethod, Object aspectTarget) {
        super(aspectMethod, aspectTarget);
    }

    public Object invoke(MyMethodInvocation mi) throws Throwable {

        try {
            return mi.proceed();
        } catch (Throwable e) {
            invokeAdviceMethod(mi,null,e.getCause());
            throw e;
        }
    }

    public void setThrowName(String throwName) {
        this.throwName = throwName;
    }
}
