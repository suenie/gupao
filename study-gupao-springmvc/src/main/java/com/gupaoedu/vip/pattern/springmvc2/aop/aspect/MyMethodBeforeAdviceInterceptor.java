package com.gupaoedu.vip.pattern.springmvc2.aop.aspect;

import com.gupaoedu.vip.pattern.springmvc2.aop.intercept.MyMethodInterceptor;
import com.gupaoedu.vip.pattern.springmvc2.aop.intercept.MyMethodInvocation;
import java.lang.reflect.Method;

public class MyMethodBeforeAdviceInterceptor extends MyAbstractAspectAdvice implements MyMethodInterceptor {

    private MyJoinPoint joinPoint;

    public MyMethodBeforeAdviceInterceptor(Method aspectMethod, Object aspectTarget) {
        super(aspectMethod, aspectTarget);
    }

    public Object invoke(MyMethodInvocation mi) throws Throwable {
        this.joinPoint = mi;
        before(mi.getMethod(),mi.getArguments(),mi.getThis());
        return mi.proceed();
    }

    private void before(Method method, Object[] arguments, Object target)throws Throwable {
        super.invokeAdviceMethod(this.joinPoint,null,null);
    }
}
