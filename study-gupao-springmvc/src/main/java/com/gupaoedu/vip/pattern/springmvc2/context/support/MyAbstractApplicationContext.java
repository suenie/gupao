package com.gupaoedu.vip.pattern.springmvc2.context.support;

/**
 * IOC容器的顶层设计
 */
public abstract class MyAbstractApplicationContext {

    protected void refresh() throws Exception {};
}
