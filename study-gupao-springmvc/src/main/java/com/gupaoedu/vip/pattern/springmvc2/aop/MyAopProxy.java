package com.gupaoedu.vip.pattern.springmvc2.aop;

public interface MyAopProxy {

    Object getProxy();

    Object getProxy(ClassLoader classLoader);
}
