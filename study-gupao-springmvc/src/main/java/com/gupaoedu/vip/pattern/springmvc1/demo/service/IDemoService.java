package com.gupaoedu.vip.pattern.springmvc1.demo.service;


public interface IDemoService {
	
	String get(String name);
	
}
