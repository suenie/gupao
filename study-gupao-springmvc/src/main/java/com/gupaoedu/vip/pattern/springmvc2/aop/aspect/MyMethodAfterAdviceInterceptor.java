package com.gupaoedu.vip.pattern.springmvc2.aop.aspect;

import com.gupaoedu.vip.pattern.springmvc2.aop.intercept.MyMethodInterceptor;
import com.gupaoedu.vip.pattern.springmvc2.aop.intercept.MyMethodInvocation;

import java.lang.reflect.Method;

public class MyMethodAfterAdviceInterceptor extends MyAbstractAspectAdvice implements MyMethodInterceptor {

    private MyJoinPoint joinPoint;

    public MyMethodAfterAdviceInterceptor(Method aspectMethod, Object aspectTarget) {
        super(aspectMethod, aspectTarget);
    }

    public Object invoke(MyMethodInvocation mi) throws Throwable {
        this.joinPoint = mi;
        Object returnVal = mi.proceed();
        this.afterReturning(returnVal,mi.getMethod(),mi.getArguments(),mi.getThis());
        return returnVal;
    }

    private void afterReturning(Object returnVal, Method method, Object[] arguments, Object aThis) throws Throwable {
        super.invokeAdviceMethod(joinPoint, returnVal, null);
    }
}
