package com.gupaoedu.vip.pattern.proxy.dbroute;

import com.gupaoedu.vip.pattern.proxy.dbroute.proxy.CglibProxyOrderService;
import com.gupaoedu.vip.pattern.proxy.dbroute.proxy.DynamicProxyOrderService;
import com.gupaoedu.vip.pattern.proxy.dbroute.proxy.OrderServiceProxy;

import java.util.Date;

public class Test {

    public static void main(String[] args) {
        Order order = new Order();
        order.setCreateTime(new Date());
        order.setId(1);
//        OrderService orderService = (OrderService) new DynamicProxyOrderService().getInstance(new OrderServiceImpl(new OrderDao()));
        OrderService orderService = (OrderService) new CglibProxyOrderService().getInstance(OrderServiceImpl.class);
        orderService.createOrder(order);
    }
}
