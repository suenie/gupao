package com.gupaoedu.vip.pattern.proxy.dynamicproxy.cglib;

public class Test {

    public static void main(String[] args) {
        Players players = (Players) new CglibDynamicProxy().getInstance(Players.class);
        players.playGame("asdfsa");
    }
}
