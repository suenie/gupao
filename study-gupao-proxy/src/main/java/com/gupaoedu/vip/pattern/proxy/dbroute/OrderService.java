package com.gupaoedu.vip.pattern.proxy.dbroute;

public interface OrderService {

    void createOrder(Order order);
}
