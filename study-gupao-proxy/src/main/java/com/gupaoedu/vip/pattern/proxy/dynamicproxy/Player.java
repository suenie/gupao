package com.gupaoedu.vip.pattern.proxy.dynamicproxy;

import com.gupaoedu.vip.pattern.proxy.Person;

public class Player implements Person {
    public void playGame(String args) {
        System.out.println("玩家要练级");
    }
}
