package com.gupaoedu.vip.pattern.proxy.dbroute.db;

public class DynamicDataSourceEntity {

    private static ThreadLocal<String> local = new ThreadLocal<String>();

    private DynamicDataSourceEntity (){}

    public String get (){return local.get();}

    //切换数据源
    public static void set(String source){ local.set(source);}

    //切换数据源
    public static void set(int year){  local.set("db_"+year);}
}
