package com.gupaoedu.vip.pattern.proxy.staticproxy;

import com.gupaoedu.vip.pattern.proxy.Person;

public class XiaoMing implements Person {

    public void playGame(String args){
        System.out.println("小明要练级");
    }
}
