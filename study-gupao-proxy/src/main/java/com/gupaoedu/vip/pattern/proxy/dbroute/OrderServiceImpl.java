package com.gupaoedu.vip.pattern.proxy.dbroute;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;

    public OrderServiceImpl() {
        this.orderDao = new OrderDao();
    }

    public void createOrder(Order order) {
        System.out.println("开始创建订单");
        orderDao.createOrder(order);
    }
}

