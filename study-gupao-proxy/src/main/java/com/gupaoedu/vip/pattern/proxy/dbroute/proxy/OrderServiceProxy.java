package com.gupaoedu.vip.pattern.proxy.dbroute.proxy;

import com.gupaoedu.vip.pattern.proxy.dbroute.Order;
import com.gupaoedu.vip.pattern.proxy.dbroute.OrderService;
import com.gupaoedu.vip.pattern.proxy.dbroute.db.DynamicDataSourceEntity;

import java.text.SimpleDateFormat;

/**
 * 通过静态代理来实现db的数据源切换，但是这个静态代理只是代理单个对象，如果是多个service就会显得不够。
 * 所以必须要用到动态代理来实现，可以在每个service执行操作之前切换数据源。
 */
public class OrderServiceProxy implements OrderService {

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy");

    private OrderService orderService;

    public OrderServiceProxy (OrderService orderService){
        this.orderService = orderService;
    }

    public void createOrder(Order order) {
        Integer dbroute = Integer.valueOf(sdf.format(order.getCreateTime()));
        System.out.println("切换数据源保存订单信息");
        DynamicDataSourceEntity.set(dbroute);
        this.orderService.createOrder(order);
    }

}
