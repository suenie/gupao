package com.gupaoedu.vip.pattern.proxy.dbroute;

import java.util.Date;

public class Order {

    private Object orderinfo;

    private Date createTime;

    private Integer id;

    public Object getOrderinfo() {
        return orderinfo;
    }

    public void setOrderinfo(Object orderinfo) {
        this.orderinfo = orderinfo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderinfo=" + orderinfo +
                ", createTime=" + createTime +
                ", id=" + id +
                '}';
    }
}
