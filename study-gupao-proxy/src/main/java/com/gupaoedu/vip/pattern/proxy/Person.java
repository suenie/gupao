package com.gupaoedu.vip.pattern.proxy;

public interface Person {

    public void playGame(String args);
}
