package com.gupaoedu.vip.pattern.proxy.staticproxy;

import com.gupaoedu.vip.pattern.proxy.Person;

//静态代理，项目编写的三层架构的dao service 的依赖注入就是静态代理。
public class XiaoHua {

    private Person person;

    public XiaoHua(XiaoMing xiaoMing){
        this.person=xiaoMing;
    }

    public void playGame(String args){
        System.out.println("小华帮小明开始练级");
        person.playGame(args);
        System.out.println("练完");
    }
}
