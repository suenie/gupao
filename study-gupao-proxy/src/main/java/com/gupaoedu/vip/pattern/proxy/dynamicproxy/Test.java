package com.gupaoedu.vip.pattern.proxy.dynamicproxy;

import com.gupaoedu.vip.pattern.proxy.Person;

public class Test {

    public static void main(String[] args) {
        Person person = (Person) new JDKProxyPlayer().getInstance(new Player());
        person.playGame("参数值");
    }
}
