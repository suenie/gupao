package com.gupaoedu.vip.singleton.test;

public class LazySingletonTest {

    public static void main(String[] args) {


        Thread t1 = new Thread(new SingletonThread());
        Thread t2 = new Thread(new SingletonThread());
        t1.start();
        t2.start();
        System.out.println("end");
    }
}
