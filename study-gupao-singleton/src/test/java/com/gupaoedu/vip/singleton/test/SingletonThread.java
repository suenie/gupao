package com.gupaoedu.vip.singleton.test;

import com.gupaoedu.vip.singleton.lazy.InnerClassSingleton;
import com.gupaoedu.vip.singleton.lazy.LazySingleton;
import com.gupaoedu.vip.singleton.lazy.LazySynchronizedSingleton;

public class SingletonThread implements Runnable {
    public void run() {
        InnerClassSingleton lazySingleton = InnerClassSingleton.getInstance();
        System.out.println(Thread.currentThread().getName()+":"+lazySingleton);
    }
}
