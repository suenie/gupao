package com.gupaoedu.vip.singleton.serializable;

import com.gupaoedu.vip.singleton.enums.EnumSingleton;
import com.gupaoedu.vip.singleton.lazy.InnerClassSingleton;

import java.io.*;
import java.lang.reflect.Constructor;

public class Test {

    public static void main(String[] args) {
        InnerClassSingleton singleton = InnerClassSingleton.getInstance();

        InnerClassSingleton singleton1 = null;
        try {
            FileOutputStream fos = new FileOutputStream(new File("singleton"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(singleton);

            FileInputStream fis = new FileInputStream(new File("singleton"));
            ObjectInputStream ois = new ObjectInputStream(fis);
            singleton1 = (InnerClassSingleton) ois.readObject();
//            Class<?> clazz = Class.forName("com.gupaoedu.vip.singleton.lazy.InnerClassSingleton");
//            Constructor c = clazz.getDeclaredConstructor();
//            c.setAccessible(true);
//            singleton1 = (InnerClassSingleton) c.newInstance();
            System.out.println(singleton);
            System.out.println(singleton1);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
