package com.gupaoedu.vip.singleton;

public class Pojo {

    private String name;

    public Pojo() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
