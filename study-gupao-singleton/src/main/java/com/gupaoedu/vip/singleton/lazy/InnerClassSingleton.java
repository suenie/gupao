package com.gupaoedu.vip.singleton.lazy;

import java.io.Serializable;

/**
 * 采用内部类来实现单例。内部类是饿汉式单例初始化，外部是懒汉式加载。
 * final 变量在初始化之后就无法被修改，那么就是线程安全的。在jvm层面保证了只会初始化一次。
 */
public class InnerClassSingleton implements Serializable {

    private InnerClassSingleton (){
        if(InnerClass.innerSingleton!=null){
            throw new RuntimeException("不允许构建多个实例");
        }
    }

    public Object readResolve(){
        return InnerClass.innerSingleton;
    }

    public static InnerClassSingleton getInstance(){
        return InnerClass.innerSingleton;
    }

    public static class InnerClass{
        private static final InnerClassSingleton innerSingleton = new InnerClassSingleton();
    }
}
