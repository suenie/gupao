package com.gupaoedu.vip.singleton.hungry;

/**
 * 饿汉式单例，浪费内存
 * 必须是final 防止通过反射来创建构建实例
 */
public class HungrySingleton {

    private static final HungrySingleton hungrySingleton = new HungrySingleton();

    private HungrySingleton(){}

    public HungrySingleton getInstance(){
        return hungrySingleton;
    }
}
