package com.gupaoedu.vip.singleton.locathreadSingleton;

//饿汉式单例
//当前线程安全，不同线程会有不同的实例，一般用于数据源切换。
public class LocalThreadSingleton {

    private  LocalThreadSingleton (){}

    private static final ThreadLocal<LocalThreadSingleton> singleton =
        new ThreadLocal<LocalThreadSingleton>() {
            @Override
            protected LocalThreadSingleton initialValue() {
                return new LocalThreadSingleton();
            }
        };

    public static LocalThreadSingleton getInstance(){
        return singleton.get();
    }
}
