package com.gupaoedu.vip.singleton.serializable;

import java.io.Serializable;

/**
 * 通过重写readResolve可以防止单例在序列化和反序列化被破坏。
 */
public class SerializableSingleton implements Serializable {

    private static SerializableSingleton singleton = new SerializableSingleton();

    private SerializableSingleton (){};

    public static SerializableSingleton getInstance(){
        return singleton;
    }

    public Object readResolve(){
        return singleton;
    }
}
