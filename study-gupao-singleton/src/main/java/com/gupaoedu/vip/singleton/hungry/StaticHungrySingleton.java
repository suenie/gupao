package com.gupaoedu.vip.singleton.hungry;

public class StaticHungrySingleton {

    private static final StaticHungrySingleton staticHungrySingleton;

    static {
        staticHungrySingleton = new StaticHungrySingleton();
    }

    private StaticHungrySingleton (){};

    public static StaticHungrySingleton getInstance(){
        return staticHungrySingleton;
    }
}
