package com.gupaoedu.vip.singleton.lazy;

/**
 * 懒汉式单例，一般简单的写法会有线程不安全的问题，通过加synchronized可以防止线程不安全的问题，但是性能会降低。
 */
public class LazySynchronizedSingleton {

    private  static LazySynchronizedSingleton lazySingleton = null;

    private LazySynchronizedSingleton(){}

    public static synchronized LazySynchronizedSingleton getInstancec(){
        if(lazySingleton==null){
            lazySingleton = new LazySynchronizedSingleton();
        }
        return lazySingleton;
    }


}
