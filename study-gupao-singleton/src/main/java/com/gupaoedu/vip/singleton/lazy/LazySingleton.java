package com.gupaoedu.vip.singleton.lazy;

import java.io.Serializable;

/**
 * 懒汉式单例，一般简单的写法会有线程不安全的问题，通过加synchronized可以防止线程不安全的问题，但是性能会降低。
 */
public class LazySingleton implements Serializable {

    private  static LazySingleton lazySingleton = null;

    private LazySingleton (){}

    public static LazySingleton getInstancec(){
        if(lazySingleton==null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }


}
