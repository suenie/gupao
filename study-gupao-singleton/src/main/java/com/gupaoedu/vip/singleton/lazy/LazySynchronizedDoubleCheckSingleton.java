package com.gupaoedu.vip.singleton.lazy;

/**
 * 懒汉式单例，一般会有线程不安全的问题，通过加synchronized可以防止线程不安全的问题，但是性能会降低。
 * 而且容易被序列化和反序列化入侵多次创建实例
 */
public class LazySynchronizedDoubleCheckSingleton {

    private  static LazySynchronizedDoubleCheckSingleton lazySingleton = null;

    private LazySynchronizedDoubleCheckSingleton(){}

    public static synchronized LazySynchronizedDoubleCheckSingleton getInstancec(){
        if(lazySingleton==null){
            lazySingleton = new LazySynchronizedDoubleCheckSingleton();
        }
        return lazySingleton;
    }


}
