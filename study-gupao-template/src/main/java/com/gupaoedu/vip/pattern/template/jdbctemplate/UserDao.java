package com.gupaoedu.vip.pattern.template.jdbctemplate;

import javax.sql.DataSource;

public class UserDao extends JdbcTemplate {
    private UserDao(DataSource dataSource) {
        super(dataSource);
    }

    public void excute(Object [] objects) throws Exception {
        super.excuteQuery(objects);
    }
}
