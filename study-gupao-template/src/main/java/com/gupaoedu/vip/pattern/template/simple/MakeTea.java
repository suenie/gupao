package com.gupaoedu.vip.pattern.template.simple;

/**
 * 模板模式，有一套固定的流程去完成，但是有的流程的实现以及结果是固定的，有的流程实现不同，但是需要这个流程先执行再进行下一个流程。
 */
public abstract class MakeTea {

    final void make(){

        // TODO 1 烧水
        this.boilWater();

        // TODO 2 拿茶叶 茶叶有不同的，
        this.takeTea();

        // TODO 3 泡茶

        this.boilTea();

        // TODO 4 完成
        this.complete();
    }

    final void complete(){
        System.out.println("完成");
    };

    final void boilTea(){
        System.out.println("泡茶");
    };

    protected abstract void takeTea();

    final void boilWater() {
        System.out.println("烧水");
    }
}
