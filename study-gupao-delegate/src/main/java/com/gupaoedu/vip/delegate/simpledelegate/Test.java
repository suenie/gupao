package com.gupaoedu.vip.delegate.simpledelegate;

import com.alibaba.fastjson.JSON;

public class Test {

    public static void main(String[] args) {
        Leader leader = new Leader();
        Leader leader1 = JSON.parseObject(JSON.toJSONString(leader),Leader.class);
        System.out.println(leader);
        System.out.println(leader1);
    }
}
