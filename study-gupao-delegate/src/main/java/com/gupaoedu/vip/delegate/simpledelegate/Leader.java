package com.gupaoedu.vip.delegate.simpledelegate;

public class Leader {

    public void play(String arg,Teacher teacher){
        teacher.play(arg);
    }
}
