package com.gupaoedu.vip.delegate.simpledelegate;

import java.util.HashMap;
import java.util.Map;

/**
 * 委派模式：基本作用负责任务的调度和分配任务，和代理模式很像，可以看作是一种特殊情况下的静态代理的全权代理，
 * 但是代理模式注重过程，而委派模式注重结果。
 *
 * 业务场景，springMVC中，请求参数有对应需要访问的uri，DispatcherServlet通过uri来分发请求到
 * 对应controller中执行，然后将最终结果返回。
 * 代理模式注重过程，比如我需要在每个controller(不只是controller，可以是任何一个实例)执行之前或者之后做一些事情，那么用动态代理。
 *  请求端 -- 发送请求--执行业务逻辑 -- 记录请求的信息，比如是查询还是修改，记录总数。--完成逻辑返回结果 --统一返回客户端。
 * 而委派模式，不注重过程，注重执行过后的结果。请求端 -- 发送请求--dispatcher--分发给不同的controller执行得到结果--统一返回客户端。
 */
public class Teacher {

    private Map<String,Person> map = new HashMap<String, Person>();

    public Teacher() {
        map.put("语文",new StudentA());
        map.put("数学",new StudentB());
        map.put("英语",new StudentC());
    }

    public void play(String arg){
        map.get(arg).play();
    }
}
